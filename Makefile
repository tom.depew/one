# OBJS : compile target
VPATH = src
SRCDIR = src
BLDDIR = build
SRCEXT = cpp

# CC : compiler
CC = g++
#CC = x86_64-w64-mingw32-g++

# COMPILER_FLAGS : compiler options
# -w suppresses all warnings
CFLAGS = -w
# CFLAGS = -Wall

# LINKER_FLAGS : required libraries
LIB = -lassimp -lSOIL -lglfw -lGL -lGLEW -std=c++11

SOURCES := $(shell find $(SRCDIR) -type f -name *.cpp)
OBJECTS := $(patsubst $(SRCDIR)/%,$(BLDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))

# TARGET : name of executable
TARGET = bin/one
# OBJS : name of object files
# OBJECTS = main.o Game.o 

# compile commands
all : $(TARGET)

# $(TARGET) : $(OBJECTS)
# 	$(CC) $(CFLAGS) -o $(TARGET) $(OBJECTS) $(LIB) 

$(TARGET) : $(OBJECTS)
	@echo " Linking..."
	@echo " $(CC) $^ -o $(TARGET) $(LIB)"; $(CC) $^ -o $(TARGET) $(LIB)


$(BLDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(BLDDIR)
	@echo " $(CC) $(CFLAGS) $(INC) -c -o $@ $<"; $(CC) $(CFLAGS) $(INC) -c -o $@ $<

# all : $(OBJS)
# 	$(CC) $(OBJS) $(CFLAGS) $(LIB) -o $(OBJ_NAME)

clean :
	@echo Cleaning up binaries and object files...
	@echo $(SOURCES)
	@echo $(OBJECTS)
	rm $(TARGET) build/*.o
	@echo ... done.

# debug : $(OBJS)
# 	@echo Running in debug mode.
# 		$(CC) $(OBJS) -g $(LIB) -o $(OBJ_NAME)
