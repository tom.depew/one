# Introduction

Initial attempt at space exploration Game Engine.

Developed in OpenGL and C++

# Dependencies

## Required Libraries:<br>
- GLEW
- GLFW
- SOIL
- ASSIMP

### On Ubuntu 14.04

sudo apt-get install libglew-dev libsoil1 libsoil-dev libassimp3 libassimp-dev libglm-dev

### To get GLFW3

sudo add-apt-repository ppa:keithw/glfw3<br>
sudo apt-get update<br>
sudo apt-get install libglfw3 libglfw3-dev
