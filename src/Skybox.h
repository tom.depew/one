#ifndef SKYBOX_H
#define SKYBOX_H

#include <GL/glew.h>
#include <vector>
#include <SOIL/SOIL.h>
#include "Shader.h"

class Skybox
{
public:
    Skybox();

    ~Skybox();

    std::vector<GLfloat> vertices;
    std::vector<const GLchar*> faces;
    GLuint cubemapTexture;
    // std::vector<GLuint> indices;
  
    void Draw(Shader shader);
    
  private:
    // Setup skybox VAO
    GLuint VAO, VBO;

    void setupMesh();
    
    GLuint loadCubemap();
    // cleanup skybox assets
    // glDeleteVertexArrays(1, &skyboxVAO);
    // glDeleteBuffers(1, &skyboxVBO);
};

#endif