#pragma once

// std includes
#include <queue>   // queue provides FIFO access
// GL includes
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>

// Define abstract player movement operations
enum Player_Movement {
    pFORWARD,
    pBACKWARD,
    pLEFT,
    pRIGHT,
    pUP,
    pDOWN,
    pBOOST,
    pLROLL,
    pRROLL
};

// Default player movement values
const GLfloat pYAW         = 0.0f;
const GLfloat pPITCH       = 0.0f;
const GLfloat pSPEED       = 40.0f;
const GLfloat pSENSITIVITY = 0.005f;
const GLfloat pZOOM        = 1.0f;   // 1 radians

// This is a Player class that processes input, calculates Euler angles and 
// appropriate glm vectors and matrices
class Player
{
public:
    // Player Attributes
    glm::vec3 Position;
    glm::vec3 Front;
    glm::vec3 Up;
    glm::vec3 Right;
    glm::vec3 WorldUp;
    glm::quat qPlayer;

    // Euler angles
    GLfloat Roll;
    GLfloat Pitch;
    GLfloat Yaw;

    // Camera options
    GLfloat MovementSpeed;
    GLfloat MouseSensitivity;
    GLfloat Zoom;

    // Constructor: vector style input
    Player(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f),
           glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f)) : 
           Front(glm::vec3(0.0f, 0.0f, -1.0f)), Right(glm::vec3(1.0f, 0.0f, 0.0f)), 
           MovementSpeed(pSPEED), MouseSensitivity(pSENSITIVITY), Zoom(pZOOM)
    {
        this->Position = position;
        this->WorldUp = up;
        this->qPlayer = glm::angleAxis(this->Roll, this->Front);
        this->updatePlayerVectors();
    }
    // Constructor: scalar style input (not used)
    Player(GLfloat posX, GLfloat posY, GLfloat posZ, GLfloat upX, GLfloat upY,
           GLfloat upZ, GLfloat yaw, GLfloat pitch) : Front(glm::vec3(1.0f, 0.0f, 0.0f)), 
           MovementSpeed(pSPEED), MouseSensitivity(pSENSITIVITY), Zoom(pZOOM)
    {
        this->Position = glm::vec3(posX, posY, posZ);
        this->WorldUp = glm::vec3(upX, upY, upZ);
        this->updatePlayerVectors();
    }

    // Calculate view matrix
    glm::mat4 getViewMatrix() {
        glm::mat4 mIdent = glm::mat4();
        glm::mat4 mRotate = glm::mat4();
        mRotate[0] = glm::vec4(this->Front, 0.0f);
        mRotate[1] = glm::vec4(this->Up, 0.0f);
        mRotate[2] = glm::vec4(this->Right, 0.0f);
        mRotate[3] = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);

        glm::mat4 mTranslate = glm::translate( mIdent, this->Position );
        glm::mat4 mTransform = mTranslate * mRotate;

        return mTransform;
    }

    // Process keyboard input
    void processKeys(Player_Movement direction, GLfloat deltaTime) {
        GLfloat velocity = this->MovementSpeed * deltaTime;
        if (direction == pFORWARD)
            this->Position += this->Front * velocity;
        if (direction == pBACKWARD)
            this->Position -= this->Front * velocity;
        if (direction == pLEFT)
            this->Position -= this->Right * velocity;
        if (direction == pRIGHT)
            this->Position += this->Right * velocity;
        if (direction == pUP)
            this->Position += this->Up * velocity;
        if (direction == pDOWN)
            this->Position -= this->Up * velocity;
        if (direction == pBOOST) {
            this->MovementSpeed = 2.5f * pSPEED;
        } else {
            this->MovementSpeed = pSPEED;
        }
        if (direction == pLROLL)
            this->Roll = -0.02f * velocity;
            this->Right = glm::rotate(this->Right, this->Roll, this->Front);
        if (direction == pRROLL)
            this->Roll = 0.04f * velocity;
            this->Right = glm::rotate(this->Right, this->Roll, this->Front);

        this->Roll = 0.0f;

        this->updatePlayerVectors();
    }

    // Process mouse input
    void processMouseMove(GLfloat xoffset, GLfloat yoffset, GLboolean constrainPitch = true) {
        xoffset *= this->MouseSensitivity;
        yoffset *= this->MouseSensitivity;

        // incremental quaternions
        GLfloat pitch = yoffset;
        GLfloat yaw = -xoffset;
        // GLfloat roll = 0.0f;

        // apply to player orientation
        this->Front = glm::rotate(this->Front, pitch, this->Right);
        this->updatePlayerVectors();
        this->Front = glm::rotate(this->Front, yaw, this->Up);
        this->Right = glm::rotate(this->Right, yaw, this->Up);
        this->updatePlayerVectors();
        // this->Front = glm::rotate(this->Front, roll, this->Front);
        // this->updatePlayerVectors();
    }

    // Process mouse scroll-wheel input
    // 1deg = 0.01745rad, 45deg = 0.7854
    void processMouseScroll(GLfloat yoffset) {
        if (this->Zoom >= 0.1745f && this->Zoom <= 1.7854f)
        this->Zoom -= 0.1 * yoffset;
        if (this->Zoom <= 0.1745f)
        this->Zoom = 0.1745f;
        if (this->Zoom >= 1.7854f)
        this->Zoom = 1.7854f;
    }

private:
    // Calculate Camera view vectors from updated Euler angles
    void updatePlayerVectors() {
        // Front
        this->Front = glm::normalize(this->Front);
        // Right
        this->Right = glm::normalize(this->Right);
        // Up
        this->Up    = glm::normalize(glm::cross(this->Right, this->Front));
    }
};
