#ifndef PLANET_H
#define PLANET_H

#define _USE_MATH_DEFINES
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <vector>
#include <cmath>
#include <map>
#include "Shader.h"
#include "Mesh.h"

struct Face {
    GLuint v1, v2, v3;

    // Constructor
    Face(GLuint a, GLuint b, GLuint c) {
        v1 = a;
        v2 = b;
        v3 = c;
    }
};

class Planet
{
public:
    // Constructor
    Planet();

    // Destructor
    ~Planet();

    std::vector<Vertex> vertices;
    std::vector<GLuint> indices;
    std::vector<Face> faces;


    void Draw(Shader shader);

private:
    // Render Data
    GLuint VAO, VBO, EBO;
    GLuint index = 0;
    std::map<uint64_t, uint32_t> middlePointIndexCache;
    GLuint recursionLevel = 3;

    // Functions
    GLuint addVertex(glm::vec3 p);

    void addFace(std::vector<Face> *F, Face f);
    GLuint getMidPoint(int p1, int p2);

    void setupMesh();
};

#endif