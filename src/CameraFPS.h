#pragma once

// Standard includes
//#include <vector>

// GL includes
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// Define abstract camera movement operations
enum Camera_Movement {
  FORWARD,
  BACKWARD,
  LEFT,
  RIGHT,
  UP,
  DOWN
};

// Default camera values
const GLfloat YAW         = -90.0f;
const GLfloat PITCH       = -10.0f;
const GLfloat SPEED       = 40.0f;
const GLfloat SENSITIVITY = 0.15f;
const GLfloat ZOOM        = 1.0f;   // 1 radians

// Abstract camera class that processes input, calculates Euler angles and appropriate glm vectors and matrices
class Camera
{
public:
  // Camera Attributes
  glm::vec3 Position;
  glm::vec3 Front;
  glm::vec3 Up;
  glm::vec3 Right;
  glm::vec3 WorldUp;
  // Euler angles
  GLfloat Yaw;
  GLfloat Pitch;
  // Camera options
  GLfloat MovementSpeed;
  GLfloat MouseSensitivity;
  GLfloat Zoom;

  // Constructor: vector style input
  Camera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), GLfloat yaw = YAW, GLfloat pitch = PITCH) : Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(SPEED), MouseSensitivity(SENSITIVITY), Zoom(ZOOM)
  {
    this->Position = position;
    this->WorldUp = up;
    this->Yaw = yaw;
    this->Pitch = pitch;
    this->updateCameraVectors();
  }
  // Constructor: scalar style input
  Camera(GLfloat posX, GLfloat posY, GLfloat posZ, GLfloat upX, GLfloat upY, GLfloat upZ, GLfloat yaw, GLfloat pitch) : Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(SPEED), MouseSensitivity(SENSITIVITY), Zoom(ZOOM)
  {
    this->Position = glm::vec3(posX, posY, posZ);
    this->WorldUp = glm::vec3(upX, upY, upZ);
    this->Yaw = yaw;
    this->Pitch = pitch;
    this->updateCameraVectors();
  }

  // Compute and return view matrix
  glm::mat4 getViewMatrix() {
    return glm::lookAt(this->Position, this->Position + this->Front, this->Up);
  }

  // Process keyboard input
  void processKeys(Camera_Movement direction, GLfloat deltaTime) {
    GLfloat velocity = this->MovementSpeed * deltaTime;
    if (direction == FORWARD)
      this->Position += this->Front * velocity;
    if (direction == BACKWARD)
      this->Position -= this->Front * velocity;
    if (direction == LEFT)
      this->Position -= this->Right * velocity;
    if (direction == RIGHT)
      this->Position += this->Right * velocity;
    if (direction == UP)
      this->Position += this->Up * velocity;
    if (direction == DOWN)
      this->Position -= this->Up * velocity;
  }

  // Process mouse input
  void processMouseMove(GLfloat xoffset, GLfloat yoffset, GLboolean constrainPitch = true) {
    xoffset *= this->MouseSensitivity;
    yoffset *= this->MouseSensitivity;

    this->Yaw   += xoffset;
    this->Yaw   = glm::mod(Yaw + xoffset, 360.f);
    this->Pitch += yoffset;

    // Make sure the screen doesn't flip at Pitch limits
    if (constrainPitch) {
      if (this->Pitch > 89.0f) this->Pitch = 89.0f;
      if (this->Pitch < -89.0f) this->Pitch = -89.0f;
    }

    // Apply rotations; update Front/Right/Up vectors
    this->updateCameraVectors();
  }

  // Process mouse scroll-wheel input
  // 1deg = 0.01745rad, 45deg = 0.7854
  void processMouseScroll(GLfloat yoffset) {
    if (this->Zoom >= 0.1745f && this->Zoom <= 1.7854f)
      this->Zoom -= 0.1 * yoffset;
    if (this->Zoom <= 0.1745f)
      this->Zoom = 0.1745f;
    if (this->Zoom >= 1.7854f)
      this->Zoom = 1.7854f;
  }

private:
  // Calculate Camera view vectors from updated Euler angles
  void updateCameraVectors() {
    // Front
    glm::vec3 front;
    front.x = cos(glm::radians(this->Yaw)) * cos(glm::radians(this->Pitch));
    front.y = sin(glm::radians(this->Pitch));
    front.z = sin(glm::radians(this->Yaw)) * cos(glm::radians(this->Pitch));
    this->Front = glm::normalize(front);
    // Right
    this->Right = glm::normalize(glm::cross(this->Front, this->WorldUp));
    // Up
    this->Up    = glm::normalize(glm::cross(this->Right, this->Front));
  }
};
