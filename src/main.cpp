// include from system libs
#include <iostream>
using namespace std;
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/ext.hpp>

#include <glm/ext.hpp>   // glm debugging
#include <SOIL/SOIL.h>

// includes from this project
#include "Game.h"
// #include "DisplayManager.h"
#include "Shader.h"
#include "PlayerQ.h"
#include "CameraQ.h"
#include "Model.h"
#include "Skybox.h"
#include "Planet.h"

// Window dimensions
const GLuint WIDTH = 1280, HEIGHT = 720;
GLFWwindow* window; // = glfwCreateWindow(WIDTH, HEIGHT, "Game Window", NULL, NULL);

// Function prototypes
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void do_move();

// Display
// DisplayManager myDisplay;

// Player
Player player(glm::vec3(0.0f, 0.0f, 0.0f));
// Player * pPlayer = &player;

// Camera
Camera camera(glm::vec3(-20.0f, 20.0f, 0.0f));
GLfloat lastX =  WIDTH  / 2.0;
GLfloat lastY =  HEIGHT / 2.0;

// Light attributes
glm::vec3 lightPos(-400.0f, 0.0f, 10.0f);

// Movement control
bool firstMouse = true;
bool keys[1024];
bool moveCamera = false;

// Timing
GLfloat deltaTime = 0.0f;   // Time between current/last frame
GLfloat lastFrame = 0.0f;   // Time of last frame

// Game one(WIDTH, HEIGHT);

int main(int argc, char* argv[]) {
    // myDisplay.createDisplay();

    // GLFW Initialization
    glfwInit();
    // Set GLFW options
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    // Create the game window object
    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "One v0.0.1", NULL, NULL);
    if (window == NULL) {
        printf("Failed to create GLFW Window\n");
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    // glfwSetWindowPos(window, 100, 100);

    // Set cursor options
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    // Register callback function for keypress and mouse
    glfwSetKeyCallback(window, key_callback);
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetMouseButtonCallback(window, mouse_button_callback);
    glfwSetScrollCallback(window, scroll_callback);

    // Set GLEW for modern OpenGL bindings and initialize
    glewExperimental = GL_TRUE;
    if (glewInit() != GLEW_OK) {
        printf("Failed to initialize GLEW\n");
        return -1;
    }

    // Define viewport dimensions
    //glfwGetFramebufferSize(window, WIDTH, HEIGHT);
    glViewport(0, 0, WIDTH, HEIGHT);

    // Set up OpenGL options
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    // glEnable(GL_CULL_FACE);  // something weird here with camera

    // // Initialize game assets
    // one.gameInit();
    // one.State = GAME_ACTIVE;

    // Set up shaders
    // vertex and fragment
    Shader shaderProgram("src/vertex.glsl", "src/fragment.glsl");
    // skybox
    Shader skyboxShader("src/skybox.vert", "src/skybox.frag");
    // planet
    Shader planetShader("src/planet.vert", "src/planet.frag");
    // light
    Shader lightShader("src/light.vert", "src/light.frag");

    // Make a skybox
    Skybox skyboxModel;

    // Make a lamp
    Planet lampModel;
    glm::mat4 modLamp;
    modLamp = glm::translate(modLamp, lightPos);
    modLamp = glm::scale(modLamp, glm::vec3(0.4f, 0.4f, 0.4f));

    // Make a planet
    Planet planetModel;
    glm::mat4 modPlanet;
    modPlanet = glm::translate(modPlanet, glm::vec3(0.0f, 4.0f, -168.0f));
    modPlanet = glm::scale(modPlanet, glm::vec3(135.4f, 135.4f, 135.4f));

    // Make a model for our player
    // Model ourModel("models/nanosuit/nanosuit.obj");
    // Model shipModel("models/space_frigate_6/space_frigate_6.obj");
    Model shipModel("assets/models/cruiser/cruiser.obj");
    // Player player(shipModel, glm::vec3(0.0f, 0.0f, 0.0f), 0.0f, 0.0f, 0.0f, 0.5f);

    glm::mat4 playerModelMatrix;
    playerModelMatrix = glm::translate(playerModelMatrix, player.Position - 20.0f*player.Up);
    playerModelMatrix = glm::scale(playerModelMatrix, glm::vec3(1.2f, 1.2f, 1.2f));
    playerModelMatrix = glm::rotate(playerModelMatrix, -1.57079673f, glm::vec3(0.0f, 1.0f, 0.0f));

    camera.initCamera(player);

    GLfloat boxVertices[] = {
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
         0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,

        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,

        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,

         0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
         0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
         0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
         0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
         0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
         0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,

        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f
    };
    GLuint boxVAO, boxVBO;
    glGenVertexArrays(1, &boxVAO);
    glGenBuffers(1, &boxVBO);
    // glGenBuffers(1, &this->EBO);

    // bind VAO
    glBindVertexArray(boxVAO);
    // bind VBO and copy vertices array into vertex buffer
    glBindBuffer(GL_ARRAY_BUFFER, boxVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(boxVertices), boxVertices, GL_STATIC_DRAW);

    // glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
    // glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof(GLuint),
    // &this->indices[0], GL_STATIC_DRAW);

    // Set vertex attribute pointers
    // Vertex Positions
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6*sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(0);
    // don't have any more right now but maybe later
    // Vertex Normals
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6*sizeof(GLfloat), (GLvoid*)(3*sizeof(GLfloat)));
    glEnableVertexAttribArray(1);

    // // Vertex Texture Coords
    // glEnableVertexAttribArray(2);
    // glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, TexCoords));

    // release VAO
    glBindVertexArray(0);
    glm::mat4 box;
    box = glm::translate(box, glm::vec3(0.0f, -10.0f, 10.0f));
    box = glm::scale(box, glm::vec3(10.2f, 10.2f, 10.2f));
    // box = glm::rotate(box, glm::radians(45.0f), glm::vec3(0.5f, 1.0f, 0.0f));

    // Rendering Loop
    while(!glfwWindowShouldClose(window)) {
    // while(!glfwWindowShouldClose(myDisplay.window)) {
        // Track Time
        GLfloat currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        // Check and call events
        glfwPollEvents();

        // update movement
        do_move();

        // Render
        // Clear Color buffer
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // basic setup
        glm::mat4 view = glm::mat4(glm::mat3(camera.getViewMatrix()));	// Remove any translation component of the view matrix
        glm::mat4 projection = glm::perspective(camera.Zoom, (GLfloat)WIDTH/(GLfloat)HEIGHT, 0.1f, 10000.0f);

        // Draw skybox first
        skyboxShader.Use();
        glUniformMatrix4fv(glGetUniformLocation(skyboxShader.Program, "view"), 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(glGetUniformLocation(skyboxShader.Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
        glUniform1i(glGetUniformLocation(skyboxShader.Program, "skybox"), 0);

        skyboxModel.Draw(skyboxShader);

        // draw
        // Activate shaderProgram
        shaderProgram.Use();
        // Create matrix transformations
        view = camera.setCameraPosition(player, deltaTime);
        // view = camera.setCameraPosition(player);
        // modShip = glm::translate(modShip, player.Position);
        playerModelMatrix = player.getViewMatrix();

        // Get uniform locations in shaders
        GLint viewLoc = glGetUniformLocation(shaderProgram.Program, "view");
        GLint projLoc = glGetUniformLocation(shaderProgram.Program, "projection");
        // Pass matrices to shaderProgram
        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));   // Normally could do this once if not changing

        //Draw the loaded model
        glUniformMatrix4fv(glGetUniformLocation(shaderProgram.Program, "model"), 1, GL_FALSE, glm::value_ptr(playerModelMatrix));
        shipModel.Draw(shaderProgram);

        // Draw the lamp
        lightShader.Use();
        glUniformMatrix4fv(glGetUniformLocation(lightShader.Program, "view"), 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(glGetUniformLocation(lightShader.Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));   // Normally could do this once if not changing
        glUniformMatrix4fv(glGetUniformLocation(lightShader.Program, "model"), 1, GL_FALSE, glm::value_ptr(modLamp));
        lampModel.Draw(lightShader);

        // Draw a planet?
        planetShader.Use();
        // Pass matrices to shaderProgram
        glUniformMatrix4fv(glGetUniformLocation(planetShader.Program, "view"), 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(glGetUniformLocation(planetShader.Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));   // Normally could do this once if not changing
        glUniformMatrix4fv(glGetUniformLocation(planetShader.Program, "model"), 1, GL_FALSE, glm::value_ptr(modPlanet));

        GLint lightPosLoc = glGetUniformLocation(planetShader.Program, "lightPos");
        glUniform3f(lightPosLoc, lightPos.x, lightPos.y, lightPos.z);
        glUniform3f(glGetUniformLocation(planetShader.Program, "lightColor"), 1.0f, 1.0f, 1.0f);

        planetModel.Draw(planetShader);

        // Draw a box
        // planetShader.Use();
        // glUniformMatrix4fv(glGetUniformLocation(planetShader.Program, "view"), 1, GL_FALSE, glm::value_ptr(view));
        // glUniformMatrix4fv(glGetUniformLocation(planetShader.Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));   // Normally could do this once if not changing
        glUniformMatrix4fv(glGetUniformLocation(planetShader.Program, "model"), 1, GL_FALSE, glm::value_ptr(box));

        // GLint lightPosLoc = glGetUniformLocation(planetShader.Program, "lightPos");
        glUniform3f(lightPosLoc, lightPos.x, lightPos.y, lightPos.z);
        glUniform3f(glGetUniformLocation(planetShader.Program, "lightColor"), 1.0f, 1.0f, 1.0f);

        // Draw the box mesh
        glBindVertexArray(boxVAO);
        glDrawArrays(GL_TRIANGLES, 0, 36);
        glBindVertexArray(0);

        // Swap buffers
        glfwSwapBuffers(window);
        // myDisplay.updateDisplay();

    }

    glm::mat4 projection = glm::perspective(camera.Zoom, (GLfloat)WIDTH/(GLfloat)HEIGHT, 0.1f, 10000.0f);
    std::cout << "view\n" << glm::to_string(camera.getViewMatrix()) << std::endl;
    std::cout << "proj\n" << glm::to_string(projection) << std::endl;
    std::cout << "player\n" << glm::to_string(playerModelMatrix) << std::endl;
    std::cout << "player position\n" << glm::to_string(player.Position) << std::endl;

    // clear GLFW resources
    glfwTerminate();
    return 0;
}

// Called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode) {
    // When a user presses the escape key, we set the WindowShouldClose property to true,
    // closing the application
    if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GL_TRUE);

    // When user presses another key
    if (key >=0 && key < 1024) {
        if(action == GLFW_PRESS)
        keys[key] = true;
        else if(action == GLFW_RELEASE)
        keys[key] = false;
    }
}

void do_move() {
    // player Controls
    if(keys[GLFW_KEY_E])
        player.processKeys(pFORWARD, deltaTime);
    if(keys[GLFW_KEY_D])
        player.processKeys(pBACKWARD, deltaTime);
    if(keys[GLFW_KEY_S])
        player.processKeys(pLEFT, deltaTime);
    if(keys[GLFW_KEY_F])
        player.processKeys(pRIGHT, deltaTime);
    if(keys[GLFW_KEY_A])
        player.processKeys(pUP, deltaTime);
    if(keys[GLFW_KEY_Z])
        player.processKeys(pDOWN, deltaTime);
    if(keys[GLFW_KEY_LEFT_SHIFT])
        player.processKeys(pBOOST, deltaTime);
    if(keys[GLFW_KEY_W])
        player.processKeys(pLROLL, deltaTime);
    if(keys[GLFW_KEY_R])
        player.processKeys(pRROLL, deltaTime);

    // Camera Controls
    // if(keys[GLFW_KEY_E])
    //   camera.processKeys(FORWARD, deltaTime);
    // if(keys[GLFW_KEY_D])
    //   camera.processKeys(BACKWARD, deltaTime);
    // if(keys[GLFW_KEY_S])
    //   camera.processKeys(LEFT, deltaTime);
    // if(keys[GLFW_KEY_F])
    //   camera.processKeys(RIGHT, deltaTime);
    // if(keys[GLFW_KEY_A])
    //   camera.processKeys(UP, deltaTime);
    // if(keys[GLFW_KEY_Z])
    //   camera.processKeys(DOWN, deltaTime);
}

// Mouse callback
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    if(firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    GLfloat xoffset = xpos - lastX;
    GLfloat yoffset = lastY - ypos;

    lastX = xpos;
    lastY = ypos;
    // cout << xoffset << " : " << yoffset << endl;

    if ( moveCamera == true ) {
        camera.processMouseMove(xoffset, yoffset);
    } else {
        player.processMouseMove(xoffset, yoffset);
    }
}

// Mouse button callback
void mouse_button_callback(GLFWwindow*, int button, int action, int mods)
{
    if (button == GLFW_MOUSE_BUTTON_RIGHT) {
        if ( action == GLFW_PRESS )
        moveCamera = true;
        else if ( action == GLFW_RELEASE )
        moveCamera = false;
    }
}

// Mouse scroll
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    camera.processMouseScroll(yoffset);
}
