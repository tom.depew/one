// just need to include the associated header?
#include "Skybox.h"

// Constructor
Skybox::Skybox() 
{
	// defined in Skybox.h
	// std::vector<GLfloat> vertices;
	// std::vector<const GLchar*> faces;
	// GLuint cubemapTexture;
	
	// Positions
	GLfloat skyboxVertices[] = {
		-1.0f,  1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		-1.0f,  1.0f, -1.0f,
		1.0f,  1.0f, -1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		1.0f, -1.0f,  1.0f
	};
	this->vertices.insert(vertices.end(), &skyboxVertices[0], &skyboxVertices[108]);

	// Load Skybox texture filenames
	this->faces.push_back("assets/models/skybox0/right.jpg");
	this->faces.push_back("assets/models/skybox0/left.jpg");
	this->faces.push_back("assets/models/skybox0/top.jpg");
	this->faces.push_back("assets/models/skybox0/bottom.jpg");
	this->faces.push_back("assets/models/skybox0/front.jpg");
	this->faces.push_back("assets/models/skybox0/back.jpg");

	// this->cubemapTexture = loadCubemap(this->faces);
	this->cubemapTexture = loadCubemap();
	
	// setup the mesh
	this->setupMesh();
}

// Destructor
Skybox::~Skybox()
{
// When do we need to write a user-defined destructor?
//
// If we do not write our own destructor in class, compiler creates a default 
// destructor for us. The default destructor works fine unless we have 
// dynamically allocated memory or pointer in class. When a class contains a 
// pointer to memory allocated in class, we should write a destructor to release
// memory before the class instance is destroyed. This must be done to avoid memory
// leak.
}

void Skybox::Draw(Shader shader)
{
	// Remember to turn depth writing off
	glDepthMask(GL_FALSE);

    // Draw mesh
    glBindVertexArray(this->VAO);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, this->cubemapTexture);
    glDrawArrays(GL_TRIANGLES, 0, this->vertices.size()/3);
    glBindVertexArray(0);

	// turn depth writing back on
    glDepthMask(GL_TRUE);
}

void Skybox::setupMesh() 
{
	// Generate vertex arrays & buffer
    glGenVertexArrays(1, &this->VAO);
    glGenBuffers(1, &this->VBO);

    // Bind VAO
    glBindVertexArray(this->VAO);

    // Bind VBO to VAO and buffer vertex data
    glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
    glBufferData(GL_ARRAY_BUFFER, this->vertices.size()*sizeof(GLfloat),
    &this->vertices[0], GL_STATIC_DRAW);

    // Set vertex attribute pointers
    // Vertex positions
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);

    // release VAO
    glBindVertexArray(0);
}

// Loads a cubemap texture from 6 individual texture faces
// Order should be:
// +X (right)
// -X (left)
// +Y (top)
// -Y (bottom)
// +Z (front)
// -Z (back)
// GLuint loadCubemap(std::vector<const GLchar*> faces)
GLuint Skybox::loadCubemap() 
{
    GLuint textureID;
    glGenTextures(1, &textureID);

    int width, height;
    unsigned char* image;

    glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);
    for(GLuint i = 0; i < this->faces.size(); i++)
    {
		image = SOIL_load_image(this->faces[i], &width, &height, 0, SOIL_LOAD_RGB);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
		SOIL_free_image_data(image);
    }
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

    return textureID;
}
// cleanup skybox assets
// glDeleteVertexArrays(1, &skyboxVAO);
// glDeleteBuffers(1, &skyboxVBO);
// };
