#pragma once
// Standard includes

// GL includes
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>

// Define abstract camera movement operations
enum Camera_Movement {
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT,
    UP,
    DOWN
};

// Default camera values
const GLfloat YAW         = 0.0f;
const GLfloat PITCH       = -25.0f;
const GLfloat SPEED       = 20.0f;
const GLfloat SENSITIVITY = 0.15f;
const GLfloat ZOOM        = 1.0f;   // 1 radians
const GLfloat ORBITRADIUS = 50.0f;

// Abstract camera class that processes input, calculates Euler angles and 
// appropriate glm vectors and matrices
class Camera
{
public:
    // Camera Attributes
    glm::vec3 Position;
    glm::vec3 targetPosition;
    glm::vec3 Front;
    glm::vec3 Up;
    glm::vec3 Right;
    glm::vec3 WorldUp;
    glm::vec3 tFront;
    
    // Euler angles
    GLfloat Yaw;
    GLfloat lastYaw;
    GLfloat Pitch;

    // Camera options
    GLfloat MovementSpeed;
    GLfloat MouseSensitivity;
    GLfloat Zoom;
    GLfloat OrbitRadius;
    // Attach to player
    // Player* player;

    // Constructor: vector style input
    Camera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f),
           glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f),
           GLfloat yaw = YAW, GLfloat pitch = PITCH, GLfloat orbitradius = ORBITRADIUS) :
           Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(SPEED),
           MouseSensitivity(SENSITIVITY), Zoom(ZOOM)
    {
        this->Position = position;
        this->WorldUp = up;
        this->Yaw = 180.f;
        this->lastYaw = this->Yaw;
        this->Pitch = 15.f;
        this->OrbitRadius = orbitradius;
        this->updateCameraVectors();
    }
    // Constructor: scalar style input
    // Camera(GLfloat posX, GLfloat posY, GLfloat posZ, GLfloat upX, GLfloat upY, GLfloat upZ,
    //        GLfloat yaw, GLfloat pitch) : Front(glm::vec3(0.0f, 0.0f, -1.0f)), 
    //        MovementSpeed(SPEED), MouseSensitivity(SENSITIVITY), Zoom(ZOOM)
    // {
    //     this->Position = glm::vec3(posX, posY, posZ);
    //     this->WorldUp = glm::vec3(upX, upY, upZ);
    //     this->Yaw = yaw;
    //     this->Pitch = pitch;
    //     this->updateCameraVectors();
    // }

    glm::mat4 setCameraPosition(Player player, GLfloat dt) {
        // this->Yaw += lag;
        GLfloat frontF = cos( glm::radians( this->Yaw )) * cos( glm::radians( this->Pitch ));
        GLfloat upF    = sin( glm::radians( this->Pitch ));
        GLfloat rightF = sin( glm::radians( this->Yaw )) * cos( glm::radians( this->Pitch ));

        // this->Position = player.Position + this->OrbitRadius *
        //     ( player.Front * frontF + player.Up * upF + player.Right * rightF );

        glm::quat cQ = glm::angleAxis( 0.0f, this->Front);
        glm::quat pQ = glm::angleAxis( 0.0f, player.Front);
        GLfloat T = 0.1f;

        // set the camera to orbit the player, from behind and above player
        this->Position = player.Position - this->OrbitRadius * (this->Front - player.Up * upF);
        // can set the center of focus back from the middle of the player
        // this->Position = (player.Position - 10.1f*player.Front) - this->OrbitRadius * 
        //     (this->Front - player.Up * upF);

        GLfloat matching = glm::dot(this->Front, player.Front);

        // point camera at player
        if (abs(matching-1.0f) < 0.000001) {
            this->Front = player.Front;
            // this->Front = glm::normalize( player.Position - this->Position );
        } else {
            this->Front = glm::slerp( this->Front, player.Front, T);
        }

        matching = glm::dot(this->Up, player.Up);

        // rotate the camera to be upright
        if (abs(matching-1.0f) < 0.000001) {
            this->Up = player.Up;
            // this->Front = glm::normalize( player.Position - this->Position );
        } else {
            this->Up = glm::slerp( this->Up, player.Up, T);
        }

        // Right
        // this->Right = glm::normalize( glm::cross( this->Front, player.Up ));
        this->Right = glm::normalize( glm::cross( this->Front, this->Up ));
        // Up
        this->Up    = glm::normalize( glm::cross( this->Right, this->Front ));

        // // calculate and return the view matrix
        // return glm::lookAt(this->Position, this->Position + this->Front, this->Up);
        return getViewMatrix();
        // return glm::lookAt( this->Position, this->Front, this->Up );
    }

    // Compute and return view matrix
    glm::mat4 getViewMatrix() {
        // Apply rotations; update Front/Right/Up vectors
        // this->updateCameraVectors();

        // this->setCameraPosition();
        return glm::lookAt(this->Position, this->Position + this->Front, this->Up);
    }

    // Process mouse input
    // will want to use this if RMB down, i.e.
    void processMouseMove(GLfloat xoffset, GLfloat yoffset, GLboolean constrainPitch = true) {
        xoffset *= this->MouseSensitivity;
        yoffset *= this->MouseSensitivity;

        // this->Yaw   += xoffset;
        this->Yaw    = glm::mod(Yaw + xoffset, 360.f);
        this->Pitch -= yoffset;

        // Make sure the screen doesn't flip at Pitch limits
        // if (constrainPitch) {
        //     if (this->Pitch > 89.0f) this->Pitch = 89.0f;
        //     if (this->Pitch < -89.0f) this->Pitch = -89.0f;
        // }

        // Apply rotations; update Front/Right/Up vectors
        // this->updateCameraVectors();
    }

    // Process mouse scroll-wheel input
    // 1deg = 0.01745rad, 45deg = 0.7854
    void processMouseScroll(GLfloat yoffset) {
        // if (this->Zoom >= 0.1745f && this->Zoom <= 1.7854f)
        //   this->Zoom -= 0.1 * yoffset;
        // if (this->Zoom <= 0.1745f)
        //   this->Zoom = 0.1745f;
        // if (this->Zoom >= 1.7854f)
        //   this->Zoom = 1.7854f;
        if (this->OrbitRadius >= 10.0f && this->OrbitRadius <= 100.0f)
            this->OrbitRadius -= 10.0f * yoffset;
        if (this->OrbitRadius <= 10.0f)
            this->OrbitRadius = 10.0f;
        if (this->OrbitRadius >= 100.0f)
            this->OrbitRadius = 100.0f;
    }

    void initCamera(Player player) {
        GLfloat frontF = cos( glm::radians( this->Yaw )) * cos( glm::radians( this->Pitch ));
        GLfloat upF    = sin( glm::radians( this->Pitch ));
        GLfloat rightF = sin( glm::radians( this->Yaw )) * cos( glm::radians( this->Pitch ));

        this->Position = player.Position + this->OrbitRadius *
            ( player.Front * frontF + player.Up * upF + player.Right * rightF );

        // point camera at player
        this->Front = glm::normalize( player.Position - this->Position );
        // Right
        this->Right = glm::normalize( glm::cross( this->Front, player.Up ));
        // Up
        this->Up    = glm::normalize( glm::cross( this->Right, this->Front ));
    }

private:
    // Calculate Camera view vectors from updated Euler angles
    void updateCameraVectors() {
        // // point camera at player
        // this->Front = glm::normalize( player.Position - this->Position );
        // // Right
        // this->Right = glm::normalize( glm::cross( this->Front, player.Up ));
        // // Up
        // this->Up    = glm::normalize( glm::cross( this->Right, this->Front ));
    }
};
