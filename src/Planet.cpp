#define _USE_MATH_DEFINES
#include "Planet.h"

// struct Face {
//     GLuint v1, v2, v3;

//     // Constructor
//     Face(GLuint a, GLuint b, GLuint c) {
//         v1 = a;
//         v2 = b;
//         v3 = c;
//     }
// };

// std::vector<Vertex> vertices;
// std::vector<GLuint> indices;
// std::vector<Face> faces;

// Constructor
Planet::Planet() 
{
	// Seed icosahedron
	GLfloat t = (1.0f+sqrt(5.0f))/2.0f;
	// const GLfloat X=0.525731112119133606f;
	// const GLfloat Z=0.850650808352039932f;
	// const GLfloat N=0.f;

	// define seed vertices
	// int index = 0;
	this->addVertex(glm::vec3(-1.0f, t, 0.0f));  // First rectangle
	this->addVertex(glm::vec3(1.0f,  t, 0.0f));
	this->addVertex(glm::vec3(-1.0f, -t, 0.0f));
	this->addVertex(glm::vec3(1.0f, -t, 0.0f));

	this->addVertex(glm::vec3(0.0f, -1.0f,  t));  // Second Rectangle
	this->addVertex(glm::vec3(0.0f,  1.0f,  t));
	this->addVertex(glm::vec3(0.0f, -1.0f, -t));
	this->addVertex(glm::vec3(0.0f,  1.0f, -t));

	this->addVertex(glm::vec3(t, 0.0f, -1.0f));  // Bottom Left
	this->addVertex(glm::vec3(t, 0.0f,  1.0f));
	this->addVertex(glm::vec3(-t, 0.0f, -1.0f));
	this->addVertex(glm::vec3(-t, 0.0f,  1.0f));

	// define faces (tris)
	this->addFace(&this->faces, Face(0, 11, 5));  // 0-center facet
	this->addFace(&this->faces, Face(0, 5, 1));
	this->addFace(&this->faces, Face(0, 1, 7));
	this->addFace(&this->faces, Face(0, 7, 10));
	this->addFace(&this->faces, Face(0, 10, 11));

	this->addFace(&this->faces, Face(1, 5, 9));   // adjacent facet
	this->addFace(&this->faces, Face(5, 11, 4));
	this->addFace(&this->faces, Face(11, 10, 2));
	this->addFace(&this->faces, Face(10, 7, 6));
	this->addFace(&this->faces, Face(7, 1, 8));

	this->addFace(&this->faces, Face(3, 9, 4));  // 3-center facet
	this->addFace(&this->faces, Face(3, 4, 2));
	this->addFace(&this->faces, Face(3, 2, 6));
	this->addFace(&this->faces, Face(3, 6, 8));
	this->addFace(&this->faces, Face(3, 8, 9));

	this->addFace(&this->faces, Face(4, 9, 5));  // adjacent facet
	this->addFace(&this->faces, Face(2, 4, 11));
	this->addFace(&this->faces, Face(6, 2, 10));
	this->addFace(&this->faces, Face(8, 6, 7));
	this->addFace(&this->faces, Face(9, 8, 1));

	// refine triangles
	for (int i = 0; i < recursionLevel; i++) {
		std::vector<Face> refinedFaces;
		for (int j = 0; j < this->faces.size(); j++) {
			// find mid points along each triangle side
			int a = getMidPoint(this->faces[j].v1, this->faces[j].v2);
			int b = getMidPoint(this->faces[j].v2, this->faces[j].v3);
			int c = getMidPoint(this->faces[j].v3, this->faces[j].v1);
			// add resulting refined triangles to new vector
			this->addFace(&refinedFaces, Face(this->faces[j].v1, a, c));
			this->addFace(&refinedFaces, Face(this->faces[j].v2, b, a));
			this->addFace(&refinedFaces, Face(this->faces[j].v3, c, b));
			this->addFace(&refinedFaces, Face(a, b, c));
		}
		// update master list of triangles
		this->faces = refinedFaces;
	}

	// add all triangles to mesh
	for (int i = 0; i < this->faces.size(); i++) {
		this->indices.push_back(this->faces[i].v1);
		this->indices.push_back(this->faces[i].v2);
		this->indices.push_back(this->faces[i].v3);
	}

	// setup the mesh
	this->setupMesh();
}

// Destructor
Planet::~Planet()
{

}

void Planet::Draw(Shader shader)
{
	// Draw mesh
	glBindVertexArray(this->VAO);
	glDrawElements(GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

// // Render Data
// GLuint VAO, VBO, EBO;
// GLuint index = 0;
// std::map<uint64_t, uint32_t> middlePointIndexCache;
// GLuint recursionLevel = 3;

// Functions
GLuint Planet::addVertex(glm::vec3 p) 
{
	Vertex vertex;
	glm::vec3 vector;

	// compute Position
	// float pMag = sqrt(p.x*p.x + p.y*p.y + p.z*p.z);
	// vector = glm::vec3( p.x/pMag, p.y/pMag, p.z/pMag );
	vector = glm::normalize(glm::vec3( p.x, p.y, p.z ));
	vertex.Position = vector;

	// compute Normal (=Position for a sphere at (0,0,0))
	vertex.Normal = vector;
	this->vertices.push_back(vertex);

	// increment the mesh vertices index
	return index++;
}

void Planet::addFace(std::vector<Face> *F, Face f)
{
	// Add the face to the list
	F->push_back(f);

	// Compute Normals
	// first get the edge vectors of the triangle face
	// glm::vec3 edge21 = (this->vertices[f.v2].Position - this->vertices[f.v1].Position);
	// glm::vec3 edge31 = (this->vertices[f.v3].Position - this->vertices[f.v1].Position);
	// glm::vec3 edge32 = (this->vertices[f.v3].Position - this->vertices[f.v2].Position);
	// then compute normals for each vertex
	// glm::vec3 n1 = glm::normalize(glm::cross(edge21,edge31));
	// glm::vec3 n2 = glm::normalize(glm::cross(edge32,-edge21));
	// glm::vec3 n3 = glm::normalize(glm::cross(-edge31,-edge32));
	// glm::vec3 n1 = glm::normalize(this->vertices[f.v1].Position);
	// glm::vec3 n2 = glm::normalize(this->vertices[f.v2].Position);
	// glm::vec3 n3 = glm::normalize(this->vertices[f.v3].Position);
	// update the normal at that vertex
	// this->vertices[f.v1].Normal += n1;
	// this->vertices[f.v2].Normal += n2;
	// this->vertices[f.v3].Normal += n3;
}

GLuint Planet::getMidPoint(int p1, int p2)
{
	// first check to see if we have this in middlePointIndexCache
	bool firstSmaller = p1 < p2;
	uint64_t iLow = firstSmaller ? p1 : p2;
	uint64_t iHigh = firstSmaller ? p2 : p1;
	// generate unique key
	uint64_t key = (iLow << 32) + iHigh;

	// check if it exists
	// int ret = this->middlePointIndexCache.find(key);
	if (this->middlePointIndexCache.find(key) != this->middlePointIndexCache.end()) {
		return this->middlePointIndexCache[key];
	}

	// create the new midpoint
	glm::vec3 point1 = this->vertices[p1].Position;
	glm::vec3 point2 = this->vertices[p2].Position;
	glm::vec3 middle = glm::vec3( (point1.x + point2.x)/2.0f,
			(point1.y + point2.y)/2.0f,
			(point1.z + point2.z)/2.0f);

	GLuint pNew = addVertex(middle);

	// store key in map
	// cout << key << " : " << pNew << endl;
	this->middlePointIndexCache.insert(std::make_pair(key, pNew));
	return pNew;
}

void Planet::setupMesh()
{
	glGenVertexArrays(1, &this->VAO);
	glGenBuffers(1, &this->VBO);
	glGenBuffers(1, &this->EBO);

	// bind VAO
	glBindVertexArray(this->VAO);
	// bind VBO and copy vertices array into vertex buffer
	glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
	glBufferData(GL_ARRAY_BUFFER, this->vertices.size()*sizeof(Vertex),
	&this->vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof(GLuint),
	&this->indices[0], GL_STATIC_DRAW);

	// Set vertex attribute pointers
	// Vertex Positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);
	// don't have any more right now but maybe later
	// Vertex Normals
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, Normal));
	// // Vertex Texture Coords
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, TexCoords));

	// release VAO
	glBindVertexArray(0);
}